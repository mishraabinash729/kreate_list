// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig : {
    apiKey: "AIzaSyCJkrE2C_H-w2T1rhESaK8y8Js2IHm4fuY",
    authDomain: "kreate-list.firebaseapp.com",
    projectId: "kreate-list",
    storageBucket: "kreate-list.appspot.com",
    messagingSenderId: "431701092145",
    appId: "1:431701092145:web:9bc4390a3050d91799b210",
    measurementId: "G-PXKDBMYRFZ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
