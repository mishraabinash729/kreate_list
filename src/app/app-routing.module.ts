import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FaqComponent } from './faq/faq.component';
// import { GetstartedComponent } from './getstarted/getstarted.component';
import { HomeComponent } from './home/home.component';
import { PricingComponent } from './pricing/pricing.component';
import { PrivacyAndPoliciesComponent } from './privacy-and-policies/privacy-and-policies.component';
import { ProjectBuyingComponent } from './project-buying/project-buying.component';
import { RefundPoliciesComponent } from './refund-policies/refund-policies.component';
import { ShippingPoliciesComponent } from './shipping-policies/shipping-policies.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { TestimonialComponent } from './testimonial/testimonial.component';
import { ThankyouComponent } from './thankyou/thankyou.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'pricing', component: PricingComponent },
  // { path: 'signup', component: GetstartedComponent },
  { path: 'thankyou', component: ThankyouComponent },
  // { path: 'aboutus',redirectTo: '/home', pathMatch: 'full' },
  { path: 'project-buying', component: ProjectBuyingComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'faq', component: FaqComponent },
  { path: 'clients', component: TestimonialComponent },
  { path: 'terms', component: TermsAndConditionsComponent },
  { path: 'privacy', component: PrivacyAndPoliciesComponent },
  { path: 'shipping-policies', component: ShippingPoliciesComponent },
  { path: 'refund-policies', component: RefundPoliciesComponent },
  { path: '**', redirectTo: '/home' },
  { path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
