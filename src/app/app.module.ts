import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PricingComponent } from './pricing/pricing.component';
// import { GetstartedComponent } from './getstarted/getstarted.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { provideFirebaseApp, getApp, initializeApp } from '@angular/fire/app';
import { environment } from 'src/environments/environment.prod';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ProjectBuyingComponent } from './project-buying/project-buying.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FaqComponent } from './faq/faq.component';
import { TestimonialComponent } from './testimonial/testimonial.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { PrivacyAndPoliciesComponent } from './privacy-and-policies/privacy-and-policies.component';
import { ShippingPoliciesComponent } from './shipping-policies/shipping-policies.component';
import { RefundPoliciesComponent } from './refund-policies/refund-policies.component';
import { OfferComponent } from './offer/offer.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PricingComponent,
    // GetstartedComponent,
    ThankyouComponent,
    AboutusComponent,
    ProjectBuyingComponent,
    ContactUsComponent,
    FaqComponent,
    TestimonialComponent,
    TermsAndConditionsComponent,
    PrivacyAndPoliciesComponent,
    ShippingPoliciesComponent,
    RefundPoliciesComponent,
    OfferComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatIconModule,
    HttpClientModule,
    provideFirebaseApp(() => initializeApp(environment.firebaseConfig))
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
