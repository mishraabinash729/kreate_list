import { Component, HostListener, OnInit } from '@angular/core';
import { OfferComponent } from './offer/offer.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'creating-list';
  // isModalOpen: any;

  constructor (public dialog: MatDialog) {
    
  }

  ngOnInit(): void {
    // this.isModalOpen = localStorage.getItem("isModalOpen");
    const myModalEl = document.getElementById('videoModal');
    if (myModalEl) {
      myModalEl.addEventListener('hidden.bs.modal', event => {
          window.location.reload();
      })
    }

    // setTimeout(() => {
    //   if (this.isModalOpen) {} else {
    //     this.openDialog('1000ms', '500ms');
    //   }
    // }, 2000);
  }

  // openDialog(enterAnimationDuration: string, exitAnimationDuration: string) {
  //   const dialogRef = this.dialog.open(OfferComponent, {
  //     id: "offerModal",
  //     backdropClass: "backdropModal",
  //     disableClose: true,
  //     autoFocus: false,
  //     enterAnimationDuration,
  //     exitAnimationDuration
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     localStorage.setItem("isModalOpen", "true");
  //     console.log(`Dialog result: ${result}`);
  //   });
  // }

  // // Listen for the beforeunload event
  // @HostListener('window:beforeunload', ['$event'])
  // clearLocalStorage(event: Event): void {
  //   localStorage.clear();
  // }
  

}
