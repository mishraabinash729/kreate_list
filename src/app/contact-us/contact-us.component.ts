import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  form: UntypedFormGroup;

  constructor(public fb: UntypedFormBuilder, private http: HttpClient, private router: Router) {
    this.form = this.fb.group({
      name: [''],
      email: [''],
      phone: [''],
      subject: [''],
      message: ['']
    });
   }

  ngOnInit(): void {
  }

  submitForm() {
    if (this.form != null){
      var formData: any = new FormData();
      const formValues = Object.values(this.form.value);
      formData.append('name', formValues[0]);
      formData.append('email', formValues[1]);
      formData.append('phone', formValues[2]);
      formData.append('subject', formValues[3]);
      formData.append('message', formValues[4]);

      this.http.post('https://script.google.com/macros/s/AKfycbzDs1U3cnA8Z4S6GMXooyHt38ycy27jTYGRiruZl6X3-Lm6qs7J9jKv/exec', formData).subscribe(
        (response) => this.router.navigate(['thankyou']),
        (error) => console.log(error)

      )
    }
  }

}
