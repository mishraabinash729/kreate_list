import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OfferComponent } from '../offer/offer.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    // setTimeout(() => {
    //   this.openDialog('1500ms', '500ms');
    // }, 2000);
  }

  // openDialog(enterAnimationDuration: string, exitAnimationDuration: string) {
  //   const dialogRef = this.dialog.open(OfferComponent, {
  //     id: "offerModal",
  //     backdropClass: "backdropModal",
  //     disableClose: true,
  //     // width: "50%",
  //     enterAnimationDuration,
  //     exitAnimationDuration
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log(`Dialog result: ${result}`);
  //   });
  // }

}
