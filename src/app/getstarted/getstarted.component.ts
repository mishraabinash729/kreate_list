import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UntypedFormBuilder, UntypedFormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-getstarted',
  templateUrl: './getstarted.component.html',
  styleUrls: ['./getstarted.component.css']
})
export class GetstartedComponent implements OnInit {

  form: UntypedFormGroup;
  referral_code: any;
  isDisabled: any;

  constructor(public fb: UntypedFormBuilder, private http: HttpClient, private router: Router, private route: ActivatedRoute) {
    this.form = this.fb.group({
      name: [''],
      email: [''],
      phone: [''],
      // subject: [''],
      // message: ['']
      mainAccountDetails: [''],
      service: [''],
      platform: [''],
      modeOfCommunication: [''],
      platformLoginCredential: [''],
      pricingLevel: [''],
      enquirySource: [''],
      salesInvolved: [''],
      referralCode: [''],
      descriptionType: ['']
    });
   }

  ngOnInit(): void {
    // var url = document.URL;
    // var index = url.indexOf( "?" ); 
    // if(index != -1){
    //   var urlParts = url.split('/');
    //   var urlPartsSecond = urlParts[3].split('?');
    //   var lastIndex = urlPartsSecond[1].split('=');
    //   this.referral_code = lastIndex[1];
    // }

    this.route.queryParams.subscribe(params => {

        this.referral_code = params['referral_code'];

        if(this.referral_code && this.referral_code != undefined){
          console.log(this.referral_code); // popular
        } else {
          this.router.navigate(['signup']);
        }

        
      }
    );
    this.isDisabled = true;
    
  }

  submitForm() {

    if(this.form != null && this.isDisabled == false){
      var formData: any = new FormData();
      // formData.append(this.form.value);
      const formValues = Object.values(this.form.value);
      formData.append('name', formValues[0]);
      formData.append('email', formValues[1]);
      formData.append('phone', formValues[2]);
      formData.append('mainAccountDetails', formValues[3]);
      formData.append('service', formValues[4]);
      formData.append('platform', formValues[5]);
      formData.append('modeOfCommunication', formValues[6]);
      formData.append('platformLoginCredential', formValues[7]);
      formData.append('pricingLevel', formValues[8]);
      formData.append('enquirySource', formValues[9]);
      formData.append('salesInvolved', formValues[10]);
      if(this.referral_code){
        formData.append('referralCode', this.referral_code);
      } else {
        formData.append('referralCode', formValues[11]);
      }
      
      formData.append('descriptionType', formValues[12]);

        this.http.post('https://script.google.com/macros/s/AKfycbwu6YVMPGx3wBizlyMPl3Fu9BQjnJ6ZjARP6YRg-HdhB9rraTBD7Xz6VvmR8WO-ZmeDvw/exec', formData).subscribe(
        (response) => this.router.navigate(['thankyou']),
        (error) => console.log(error)
      )
    } else {
      alert("Please agree to all the policies for registered the account");
    } 
  }

  checkCheckBoxvalue(event: any){
    console.log(event.srcElement.checked)
    if(event.srcElement.checked == true){
      this.isDisabled = false;
    } else {
      this.isDisabled = true;
    }
  }

  // submitForm() {
  //   if (this.form != null){
  //     var formData: any = new FormData();
  //     const formValues = Object.values(this.form.value);
  //     formData.append('name', formValues[0]);
  //     formData.append('email', formValues[1]);
  //     formData.append('phone', formValues[2]);
  //     formData.append('subject', formValues[3]);
  //     formData.append('message', formValues[4]);

  //     this.http.post('https://script.google.com/macros/s/AKfycbzDs1U3cnA8Z4S6GMXooyHt38ycy27jTYGRiruZl6X3-Lm6qs7J9jKv/exec', formData).subscribe(
  //       (response) => this.router.navigate(['thankyou']),
  //       (error) => console.log(error)

  //     )
  //   }
  // }

}
