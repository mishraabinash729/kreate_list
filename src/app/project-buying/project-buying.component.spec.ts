import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectBuyingComponent } from './project-buying.component';

describe('ProjectBuyingComponent', () => {
  let component: ProjectBuyingComponent;
  let fixture: ComponentFixture<ProjectBuyingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectBuyingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectBuyingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
